import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'atsistemas-breeds-carlosalurban';

  langs: string[] = [];

  constructor(private translate: TranslateService) {
    this.translate.setDefaultLang('en');
    this.translate.addLangs(['en', 'es']);
    this.langs = this.translate.getLangs();
  }

  changeToLanguageSelected(langSelection: string) {
    this.translate.use(langSelection);
  }
}
