import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Input() langs: string[];
  @Output() langSelected = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  changeLanguage(lang: string) {
    this.langSelected.emit(lang);
  }

}
