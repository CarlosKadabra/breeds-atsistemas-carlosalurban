import { Component, Input, OnInit } from '@angular/core';
import { AnimalsList } from 'src/app/services/breeds';

@Component({
  selector: 'app-breed-selector',
  templateUrl: './breed-selector.component.html',
  styleUrls: ['./breed-selector.component.scss']
})
export class BreedSelectorComponent implements OnInit {
  @Input() breedList: AnimalsList;

  constructor() { }

  ngOnInit(): void {
  }

}
