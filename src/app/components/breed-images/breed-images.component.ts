import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgbCarousel, NgbSlideEvent, NgbSlideEventSource } from '@ng-bootstrap/ng-bootstrap';
import { BreedsApiImagesResponse } from 'src/app/services/breeds';

@Component({
  selector: 'app-breed-images',
  templateUrl: './breed-images.component.html',
  styleUrls: ['./breed-images.component.scss']
})
export class BreedImagesComponent implements OnInit {
  @Input() imageList: BreedsApiImagesResponse;
  @Input() breedName: string;

  constructor() { }

  ngOnInit(): void {
  }

}
