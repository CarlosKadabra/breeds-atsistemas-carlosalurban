import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;
  let translateService: TranslateService;

  const translateServiceMock = {
    setDefaulLang: () => { },
    addLangs: () => { },
    getLangs: () => { },
    use: () => { }
  }


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        {
          provider: TranslateService,
          useValue: translateServiceMock
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    translateService = fixture.debugElement.injector.get(TranslateService);

  })

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it(`should have as title 'atsistemas-breeds-carlosalurban'`, () => {
    expect(component.title).toEqual('atsistemas-breeds-carlosalurban');
  });

  it('should call use method from translate service when change changeToLanguageSelected', () => {
    const useSpy = spyOn(translateService, 'use');
    component.changeToLanguageSelected('es')
    expect(useSpy).toHaveBeenCalled();
    expect(useSpy).toHaveBeenCalledWith('es');
  })

});
