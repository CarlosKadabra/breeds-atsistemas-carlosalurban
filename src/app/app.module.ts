/**MODULES**/
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

/**COMPONENTS**/
import { AppComponent } from './app.component';
import { BreedsRouterComponent } from './routes-components/breeds-router/breeds-router.component';
import { HomeRouterComponent } from './routes-components/home-router/home-router.component';
import { BreedSelectorComponent } from './components/breed-selector/breed-selector.component';
import { BreedImagesComponent } from './components/breed-images/breed-images.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { ErrorRouterComponent } from './routes-components/error-router/error-router.component';

/**
 * Loader json translations
 * @param http 
 * @returns Json with language
 */

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/translations/', '.json');
}


@NgModule({
  declarations: [
    AppComponent,
    BreedsRouterComponent,
    HomeRouterComponent,
    BreedSelectorComponent,
    BreedImagesComponent,
    NavbarComponent,
    FooterComponent,
    ErrorRouterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    CommonModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
