import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'ngx-translate-testing';

import { ErrorRouterComponent } from './error-router.component';

describe('ErrorRouterComponent', () => {
  let component: ErrorRouterComponent;
  let fixture: ComponentFixture<ErrorRouterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ErrorRouterComponent],
      imports: [
        TranslateTestingModule.withTranslations({}),
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
