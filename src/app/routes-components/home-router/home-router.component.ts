import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AnimalsList } from 'src/app/services/breeds';
import { BreedsService } from 'src/app/services/breeds.service';

@Component({
  selector: 'app-home-router',
  templateUrl: './home-router.component.html',
  styleUrls: ['./home-router.component.scss']
})
export class HomeRouterComponent implements OnInit {

  breedsList$: Observable<AnimalsList>;

  constructor(private breedsService: BreedsService) { }

  ngOnInit(): void {
    this.breedsList$ = this.breedsService.getBreedList();
  }

}
