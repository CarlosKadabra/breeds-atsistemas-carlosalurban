import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { BreedsService } from 'src/app/services/breeds.service';
import { BreedsServiceMock } from 'src/app/services/breeds.service.mock';

import { HomeRouterComponent } from './home-router.component';

describe('HomeRouterComponent', () => {
  let component: HomeRouterComponent;
  let fixture: ComponentFixture<HomeRouterComponent>;
  let breedService: BreedsService;
  let getBreedListSpy: jasmine.Spy;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeRouterComponent],
      imports: [
        TranslateTestingModule.withTranslations({}),
        RouterTestingModule
      ],
      providers: [
        {
          provide: BreedsService,
          useClass: BreedsServiceMock
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeRouterComponent);
    component = fixture.componentInstance;
    breedService = fixture.debugElement.injector.get(BreedsService);
    getBreedListSpy = spyOn(breedService, 'getBreedList')
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call BreedList when init component', () => {
    expect(getBreedListSpy).toHaveBeenCalled();
  })
});
