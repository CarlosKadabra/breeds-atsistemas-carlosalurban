import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { BreedsService } from 'src/app/services/breeds.service';
import { BreedsServiceMock } from 'src/app/services/breeds.service.mock';
import { BreedsRouterComponent } from './breeds-router.component';


describe('BreedsRouterComponent', () => {
  let component: BreedsRouterComponent;
  let fixture: ComponentFixture<BreedsRouterComponent>;
  let breedService: BreedsService;
  let getBreedImageListSpy: jasmine.Spy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BreedsRouterComponent],
      imports: [
        TranslateTestingModule.withTranslations({}),
        RouterTestingModule,
      ],
      providers: [
        {
          provide: BreedsService,
          useClass: BreedsServiceMock
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreedsRouterComponent);
    component = fixture.componentInstance;
    breedService = fixture.debugElement.injector.get(BreedsService);
    getBreedImageListSpy = spyOn(breedService, 'getBreedImages')
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  xit('should call getBreedList when init component', () => {
    expect(getBreedImageListSpy).toHaveBeenCalled();
  })
});
