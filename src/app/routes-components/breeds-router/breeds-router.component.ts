import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EMPTY, NEVER, Observable, of, Subject } from 'rxjs';
import { catchError, delay, switchMap } from 'rxjs/operators';
import { BreedsService } from 'src/app/services/breeds.service';

@Component({
  selector: 'app-breeds-router',
  templateUrl: './breeds-router.component.html',
  styleUrls: ['./breeds-router.component.scss']
})
export class BreedsRouterComponent implements OnInit {
  breedsImageList$: Observable<string[]>;
  breedName: string;
  error$: Observable<boolean>;
  private errorSubject = new Subject<boolean>();

  constructor(private activatedRouter: ActivatedRoute, private breedsService: BreedsService) { }

  ngOnInit(): void {
    this.error$ = this.errorSubject.asObservable();
    this.breedsImageList$ = this.activatedRouter.params
      .pipe(
        //this pipe needs a delay to skip null console error because on init returns null then true or false
        delay(0),
        switchMap(({ breed }) => {
          this.errorSubject.next(false);
          this.breedName = breed;
          return this.breedsService.getBreedImages(breed)
            .pipe(
              catchError((err) => {
                this.errorSubject.next(true);
                return EMPTY;
              })
            );
        }),
      );
  }

}
