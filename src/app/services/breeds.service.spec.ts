import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AnimalsList } from './breeds';
import { BreedsService } from './breeds.service';

describe('BreedsService', () => {
  let service: BreedsService;
  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule

      ],
      providers: [BreedsService]
    });
    // service = TestBed.inject(BreedsService);
    injector = getTestBed();
    service = injector.get(BreedsService);
    httpMock = injector.get(HttpTestingController);
  });
  afterEach(() => {
    httpMock.verify();
  });

  it('should return an Oservable<AnimalList>', () => {

    const apiList = [{
      breed: "dog",
      subBreeds: ["gray", "blue"]
    },
    {
      breed: "cat",
      subBreeds: ["gray", "blue"]
    },
    ]

    service.getBreedList().subscribe(list => {
      expect(list).toEqual(apiList);
    });
    const req = httpMock.expectOne(`https://dog.ceo/api/breeds/list/all`);
    expect(req.request.method).toBe("GET");
    req.flush(apiList)
  });

  xit("should return Observable<string[]>", () => {
    const imagesList = ["image1", "image2", "image3"];


    service.getBreedImages("boxer").subscribe(img => {
      expect(img).toEqual(imagesList);
    });
    const req = httpMock.expectOne(`https://dog.ceo/api/breed/boxer/images`);
    expect(req.request.method).toBe("GET");
    req.flush(imagesList)
  })


  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
