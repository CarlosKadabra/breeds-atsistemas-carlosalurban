import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { map } from "rxjs/operators";
import { AnimalsList, BreedsApiImagesResponse, BreedsApiListResponse, IBreedsService } from './breeds';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BreedsService implements IBreedsService {

  private url: String = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getBreedList(): Observable<AnimalsList> {
    return this.http.get<BreedsApiListResponse>(`${this.url}breeds/list/all`)
      .pipe(map(({ message }) => this.mapBreedsResponse(message)));
  }

  getBreedImages(id: string): Observable<string[]> {
    //Replace '-' by '/' because subBreeds needs aditional router param
    return this.http.get<BreedsApiImagesResponse>(`${this.url}breed/${id.replace('-', '/')}/images`)
      .pipe(map(({ message }) => message));
  }

  private mapBreedsResponse(breedMap: Map<string, string[]>) {
    return Object.entries(breedMap)
      .map(([breed, subBreeds]: [string, string[]]) => ({ breed, subBreeds }))
  }
}
