import { Observable } from "rxjs";

export interface BreedsApiListResponse {
    message: Map<string, string[]>
}
export interface BreedsApiImagesResponse {
    message: string[];
}

export type AnimalsList = Breed[];

export interface Breed {
    breed: string,
    subBreeds: string[]
}

export interface IBreedsService {
    getBreedList(): Observable<AnimalsList>;
    getBreedImages(id: string): Observable<string[]>;
}