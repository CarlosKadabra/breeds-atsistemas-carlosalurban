import { Observable, of } from "rxjs";
import { AnimalsList, IBreedsService } from "./breeds";

export class BreedsServiceMock implements IBreedsService {
    getBreedList(): Observable<AnimalsList> {
        return of([]);
    }
    getBreedImages(id: string): Observable<string[]> {
        return of([]);
    }

}