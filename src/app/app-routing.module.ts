import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BreedsRouterComponent } from './routes-components/breeds-router/breeds-router.component';
import { ErrorRouterComponent } from './routes-components/error-router/error-router.component';
import { HomeRouterComponent } from './routes-components/home-router/home-router.component';

const routes: Routes = [
  {
    path: '', component: HomeRouterComponent, children: [
      { path: ':breed', component: BreedsRouterComponent }
    ]
  },
  { path: '**', component: ErrorRouterComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }